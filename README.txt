0. Preamble
Skip to point 2, if you just want to get it running.
Including an iframe for newsletter registration is not as flexible as many 
administrators of Drupal-sites needs it, due to its CSS limitations of 
rapidmail. Of course rapidmail provides basic-form includes but you have 
to deal with its tricky validation. Save some hours of troubleshooting and 
try this module!

1. What this module does
This module is an implemantation of the rapidmail.com - API for Drupal. 
It provides a basic administration menu and the possibility to place a 
user-registration block within your template.
There are three field types with mandatory options for now. 
(See planned features...)

2. How to use
Using this module is really simple. Just download and activate it. 
You have to download the rapidmail.class.php directly from your 
rapidmail account, too and put it into the "includes"-folder of this module.
After that feel free to fill in your rapidmail api key, node id and the id of
your receipient list. 
You can get all this information by browsing the api-menue in your 
rapidmail account. Don't hesitate to contact rapidmail support, if you can't
find the appropriate options. Of course you can contact project maintainers,
too!

3. planned features
- flexible adding/administration of fields
- multiple newsletter support
- add/edit users through drupal interface
- recipient lists in drupal interface
- newsletter statistics in drupal interface 
